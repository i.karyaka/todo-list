//
//  BaseError.swift
//  ToDo
//
//  Created by krazke on 6/13/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import Foundation

protocol BaseError: Error {
    var description: String { get }
}
