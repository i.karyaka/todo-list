//
//  TabBarViewController.swift
//  ToDo
//
//  Created by Igor Dev on 6/19/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import RxSwift
import LifetimeTracker

class TabBarViewController<R: Router, VM: TabBarViewModel<R>>: UITabBarController, LifetimeTrackable {

    static var lifetimeConfiguration: LifetimeConfiguration {
        return LifetimeConfiguration(maxCount: 1, groupName: "NavigationViewController")
    }

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    let disposeBag = DisposeBag()

    let viewModel: VM

    required init?(coder _: NSCoder) {
        fatalError()
    }

    init(viewModel: VM) {
        self.viewModel = viewModel
        super.init(nibName: nil,
                   bundle: nil)
        setViewControllers(viewModel.viewControllers,
                           animated: false)
        self.viewModel.router.viewController = self
        trackLifetime()
    }}
