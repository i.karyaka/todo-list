//
//  Router.swift
//  ToDo
//
//  Created by krazke on 6/13/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import RxSwift
import RxCocoa
import LifetimeTracker
import SwiftMessages

class Router: NSObject, LifetimeTrackable {

    static var lifetimeConfiguration: LifetimeConfiguration {
        var maxCount = 1
        if NSStringFromClass(self).lowercased().contains("container") {
            maxCount = 999
        }
        return LifetimeConfiguration(maxCount: maxCount, groupName: "Router")
    }

    let disposeBag = DisposeBag()

    weak var viewController: UIViewController?
    var errorHandler = ErrorHandler.shared

    var navigationAvailable = BehaviorRelay(value: true)

    required override init() {
        super.init()
        errorHandler.router = self
        navigationAvailable.accept(true)
        trackLifetime()
    }

    func show(_ message: String, style: Theme = .error) {
        let view = MessageView.viewFromNib(layout: .cardView)
        let title = (style == .error) ? R.string.localizable.errorTitle() : R.string.localizable.errorWarningTitle()
        view.configureContent(title: title,
                              body: message,
                              iconImage: nil,
                              iconText: nil,
                              buttonImage: nil,
                              buttonTitle: nil,
                              buttonTapHandler: nil)
        view.configureTheme(style, iconStyle: .subtle)
        view.accessibilityPrefix = (style == .error) ? "error" : "warning"
        view.configureDropShadow()
        view.button?.isHidden = true

        var config = SwiftMessages.defaultConfig
        config.presentationContext = .window(windowLevel: UIWindow.Level.normal)
        config.duration = .seconds(seconds: 3)
        config.dimMode = .gray(interactive: true)
        config.preferredStatusBarStyle = .lightContent
        config.shouldAutorotate = true
        config.interactiveHide = true

        SwiftMessages.show(config: config, view: view)
    }
}
