//
//  ViewController.swift
//  ToDo
//
//  Created by krazke on 6/13/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import RxSwift
import RxCocoa
import RxGesture
import RxKeyboard
import LifetimeTracker
import SnapKit

class ViewController<R: Router, VM: ViewModel<R>>: UIViewController, LifetimeTrackable {

    static var lifetimeConfiguration: LifetimeConfiguration {
        var maxCount = 1
        if NSStringFromClass(self).lowercased().contains("container") {
            maxCount = 999
        }
        return LifetimeConfiguration(maxCount: maxCount, groupName: "ViewController")
    }

    let disposeBag = DisposeBag()

    let viewModel: VM

    required init?(coder _: NSCoder) {
        fatalError()
    }

    init(viewModel: VM) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.viewModel.router.viewController = self
        trackLifetime()
    }

    override func loadView() {
        super.loadView()
        view.backgroundColor = .white
        setupConstraints()
        localize()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupRx()
        setupDataSource()
        setupTable()
        setupCollection()
    }

    func setupConstraints() {}
    func setupView() {}
    func setupRx() {}
    func setupDataSource() {}
    func setupTable() {}
    func setupCollection() {}
    func localize() {}

    final func listenKeyboardUpdates() {
        let keyboardHidden: BehaviorRelay<Bool> = BehaviorRelay(value: true)

        disposeBag.insert(
            RxKeyboard.instance.isHidden.drive(onNext: { isHidden in
                keyboardHidden.accept(isHidden)
            }),
            view.rx.tapGesture()
                .subscribe(onNext: { [weak self] _ in
                    guard !keyboardHidden.value else { return }
                    self?.view.endEditing(true)
                })
        )
    }
}
