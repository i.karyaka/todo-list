//
//  ErrorHandler.swift
//  ToDo
//
//  Created by krazke on 6/13/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import RxSwift
import Action

final class ErrorHandler {

    private let disposeBag = DisposeBag()
    static let shared = ErrorHandler()
    weak var router: Router?

    var actionErrors = PublishSubject<ActionError>()
    var serviceErrors = PublishSubject<ServiceError>()
    var warnings = PublishSubject<Warning>()

    #warning("TODO: Add logger to ErrorHandler")

    private init() {
        actionErrors
            .subscribe(onNext: { [weak self] (error) in
                if case ActionError.underlyingError(let error) = error {
                    self?.router?.show(error.localizedDescription)
                    print(error.localizedDescription)
                }
            })
            .disposed(by: disposeBag)

        serviceErrors
            .subscribe(onNext: { [weak self] (error) in
                self?.router?.show(error.description)
                print(error.description)
            })
            .disposed(by: disposeBag)

        warnings
            .subscribe(onNext: { [weak self] (warning) in
                self?.router?.show(warning.description, style: .warning)
                print(warning.description)
            })
            .disposed(by: disposeBag)
    }
}
