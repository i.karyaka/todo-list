//
//  TabBarViewModel.swift
//  ToDo
//
//  Created by Igor Dev on 6/19/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import RxSwift
import LifetimeTracker

class TabBarViewModel<R: Router>: NSObject, LifetimeTrackable {

    static var lifetimeConfiguration: LifetimeConfiguration {
        return LifetimeConfiguration(maxCount: 1, groupName: "NavigationViewModel")
    }

    let disposeBag = DisposeBag()

    let router: R
    let viewControllers: [UIViewController]

    init(viewControllers: [UIViewController]) {
        router = R()
        self.viewControllers = viewControllers
        super.init()
        trackLifetime()
    }
}
