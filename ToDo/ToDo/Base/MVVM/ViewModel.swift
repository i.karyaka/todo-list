//
//  ViewModel.swift
//  ToDo
//
//  Created by krazke on 6/13/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import RxSwift
import LifetimeTracker

class ViewModel<R: Router>: NSObject, LifetimeTrackable {

    static var lifetimeConfiguration: LifetimeConfiguration {
        var maxCount = 1
        if NSStringFromClass(self).lowercased().contains("container") {
            maxCount = 999
        }
        return LifetimeConfiguration(maxCount: maxCount, groupName: "ViewModel")
    }

    let disposeBag = DisposeBag()

    let services = Services.shared
    let router: R

    override init() {
        router = R()
        super.init()
        trackLifetime()
    }
}
