//
//  RoundedButton.swift
//  ToDo
//
//  Created by Igor Dev on 6/18/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import UIKit

final class RoundedButton: UIButton {

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height / 2
    }
}
