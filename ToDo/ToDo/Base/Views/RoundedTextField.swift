//
//  RoundedTextField.swift
//  ToDo
//
//  Created by Igor Dev on 6/17/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import UIKit

class RoundedTextField: UITextField {

    enum Style {
        case `default`
        case bordered
    }

    // MARK: - Private parameters

    private let padding = UIEdgeInsets(top: 0, left: 3, bottom: 0, right: 3)
    private var style: Style = .default

    // MARK: - Public parameters

    var cornerRadius: CGFloat = 10 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }

    var fontSize: CGFloat = 14 {
        didSet {
            self.font = .helveticaFontOf(size: fontSize)
        }
    }

    var placeholderColor: UIColor = .gray {
        didSet {
            guard let placeholderText = placeholder else { return }
            attributedPlaceholder = NSAttributedString(
                string: placeholderText,
                attributes: [NSAttributedString.Key.foregroundColor: placeholderColor]
            )
        }
    }

    var borderWidth: CGFloat = 0 {
        didSet {
            guard style == .bordered else { return }
            layer.borderWidth = borderWidth
        }
    }

    var borderColor: UIColor = .clear {
        didSet {
            guard style == .bordered else { return }
            layer.borderColor = borderColor.cgColor
        }
    }

    // MARK: - Initialization

    required init?(coder aDecoder: NSCoder) {
        fatalError("\(RoundedTextField.self) init(coder:) hasn't implemented")
    }

    init(style: Style) {
        super.init(frame: .zero)
        self.style = style
        setupView()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    private func setupView () {
        // todo: - check does font size default value triggered did set after initialization
        self.font = .helveticaFontOf(size: 14)
        self.layer.cornerRadius = cornerRadius
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
