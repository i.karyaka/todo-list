//
//  UIFont.swift
//  ToDo
//
//  Created by Igor Dev on 6/17/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import UIKit

extension UIFont {

    class func helveticaFontOf(size: CGFloat) -> UIFont {
        return UIFont(name: "Helvetica", size: size)!
    }

    class func helveticaBoldFontOf(size: CGFloat) -> UIFont {
        return UIFont(name: "Helvetica-Bold", size: size)!
    }
}
