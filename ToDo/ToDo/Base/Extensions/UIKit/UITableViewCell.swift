//
//  UITableViewCell.swift
//  ToDo
//
//  Created by Igor Dev on 6/19/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import UIKit

extension UITableViewCell {

    static var identifier: String {
        return String(describing: self)
    }
}
