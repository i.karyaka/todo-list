//
//  UITableView+Rx.swift
//  ToDo
//
//  Created by Igor Dev on 6/19/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import RxSwift
import RxCocoa

extension Reactive where Base: UITableView {

    /// Bindable sink for cell types registration.
    var cellTypes: Binder<[AnyClass]> {
        return Binder(self.base) { tableView, cells in
            for cell in cells {
                tableView.register(cell.self as AnyClass,
                                   forCellReuseIdentifier: String(describing: cell.self))
            }
        }
    }
}
