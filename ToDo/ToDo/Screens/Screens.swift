//
//  Screens.swift
//  ToDo
//
//  Created by krazke on 6/13/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import UIKit

struct Screens {

    static func navigation(root: UIViewController) -> MainNavigationViewController {
        let model = MainNavigationViewModel(root: root)
        return MainNavigationViewController(viewModel: model)
    }

    static func loginNavigation() -> MainNavigationViewController {
        let model = MainNavigationViewModel(root: login())
        return MainNavigationViewController(viewModel: model)
    }

    static func login() -> LoginViewController {
        let model = LoginViewModel()
        return LoginViewController(viewModel: model)
    }

    static func taskList() -> TaskListViewController {
        let model = TaskListViewModel()
        return TaskListViewController(viewModel: model)
    }

    static func calendar() -> CalendarViewController {
        let model = CalendarViewModel()
        return CalendarViewController(viewModel: model)
    }

    static func createTask() -> CreateTaskViewController {
        let model = CreateTaskViewModel()
        return CreateTaskViewController(viewModel: model)
    }

    static func userInfo() -> UserInfoViewController {
        let model = UserInfoViewModel()
        return UserInfoViewController(viewModel: model)
    }

    static func settings() -> SettingsViewController {
        let model = SettingsViewModel()
        return SettingsViewController(viewModel: model)
    }

    static func taskDetail(_ task: Task) -> UIViewController {
        #warning("rework it")
        return UIViewController()
    }
}
