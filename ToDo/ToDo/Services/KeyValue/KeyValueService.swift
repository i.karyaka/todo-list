//
//  KeyValueService.swift
//  ToDo
//
//  Created by krazke on 6/13/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

protocol KeyValueService: Service {

    func setValue(_ value: Any, forKey key: String)
    func value<T>(forKey key: String) throws -> T
    func deleteValue(forKey key: String)
}
