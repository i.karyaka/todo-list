//
//  UserDefaultsService.swift
//  ToDo
//
//  Created by krazke on 6/13/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import Foundation
import LifetimeTracker

final class UserDefaultsService: NSObject, KeyValueService {

    static var lifetimeConfiguration: LifetimeConfiguration {
        return LifetimeConfiguration(maxCount: 1, groupName: "UserDefaultsService")
    }

    enum Error: ServiceError {
        case valueNotFound(forKey: String)
        case some(Swift.Error)

        var serviceName: String { return "UserDefaultsService" }
        var description: String {
            switch self {
            case .valueNotFound(let key): return "Value for key \"\(key)\" not found."
            case .some(let error): return error.localizedDescription
            }
        }
    }

    let defaults = UserDefaults.standard

    override init() {
        super.init()
        trackLifetime()
    }

    func setValue(_ value: Any, forKey key: String) {
        defaults.set(value, forKey: key)
    }

    func value<T>(forKey key: String) throws -> T {
        if let value = defaults.value(forKey: key) as? T {
            return value
        }

        throw Error.valueNotFound(forKey: key)
    }

    func deleteValue(forKey key: String) {
        defaults.removeObject(forKey: key)
    }
}
