//
//  Services.swift
//  ToDo
//
//  Created by krazke on 6/13/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

final class Services {

    static let shared = Services()

    private init() {}

    // MARK: - Private services

    // MARK: - Public services

    lazy var userDefaults: KeyValueService = UserDefaultsService()
    lazy var auth: AuthService = AuthServiceV1()
}
