//
//  AuthServiceV1.swift
//  ToDo
//
//  Created by Igor Dev on 6/18/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import RxSwift
import RxCocoa
import LifetimeTracker

private enum Error: ServiceError {
    case userDoesNotExist(withMail: String)
    case some(Swift.Error)

    var serviceName: String { return "AuthService" }
    var description: String {
        switch self {
        case .userDoesNotExist(let email): return "User with email \"\(email)\" does not exist."
        case .some(let error): return error.localizedDescription
        }
    }
}

class AuthServiceV1: NSObject, AuthService {

    static var lifetimeConfiguration: LifetimeConfiguration {
        return LifetimeConfiguration(maxCount: 1, groupName: "AuthService")
    }

    var user: BehaviorRelay<User?> = BehaviorRelay(value: nil)

    func logIn(email: String) -> Observable<User> {
        return .just(User(email: "test", uid: "1"))
    }
}
