//
//  AuthService.swift
//  ToDo
//
//  Created by Igor Dev on 6/18/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import RxSwift

protocol AuthService: Service {
    func logIn(email: String) -> Observable<User>
}
