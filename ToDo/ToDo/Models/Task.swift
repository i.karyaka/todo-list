//
//  Task.swift
//  ToDo
//
//  Created by Igor Dev on 6/19/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import Foundation

protocol BaseTask: Decodable {
    var title: String { get set }
}

// MARK: - Task Objects

struct Task: BaseTask {
    var title: String
    var date: Date?
    var description: String?
    var color: String
    var priority: String
    var subtasks: [Task]?
    var location: String?

    enum CodingKeys: String, CodingKey {
        case title
        case date
        case description
        case color
        case priority
        case subtasks
        case location
    }

    mutating func setTitle(_ title: String) {
        self.title = title
    }

    mutating func setDate(_ date: Date) {
        self.date = date
    }

    mutating func setDescription(_ description: String) {
        self.description = description
    }

    mutating func setColor(_ color: String) {
        self.color = color
    }

    mutating func setPriority(_ priority: String) {
        self.priority = priority
    }

    mutating func setSubtasks(_ subtasks: [Task]) {
        self.subtasks = subtasks
    }

    mutating func setLocation(_ location: String) {
        self.location = location
    }
}

// MARK: - Decoding

private enum TaskWarning: Warning {
    case undetectedTasks([String])

    var description: String {
        switch self {
        case .undetectedTasks(let tasks): return "Can`t decode tasks: \(tasks.joined(separator: ", "))"
        }
    }
}

extension Dictionary {

    func decodeAsTasks() -> [BaseTask] {
        var tasks: [BaseTask] = []
        var undetectedKeys: [String] = []
        let jsonDecoder = JSONDecoder()

        defer {
            print("""
                Detected scenarios: \(tasks.map { $0.title }.joined(separator: ", "))
                Undetected scenarios: \(undetectedKeys.joined(separator: ", "))
                """)
            if !undetectedKeys.isEmpty {
                ErrorHandler.shared.warnings.on(.next(TaskWarning.undetectedTasks(undetectedKeys)))
            }
        }

        forEach {
            guard let value = $0.value as? [String: Any],
                let data = value.jsonData else { return }
            if var defaultTask = try? jsonDecoder.decode(Task.self, from: data) {
                defaultTask.setTitle("\($0.key)")
                tasks.append(defaultTask)
                return
            } else {
                undetectedKeys.append("\($0.key)")
            }
        }

        return tasks
    }
}
