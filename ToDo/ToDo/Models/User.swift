//
//  User.swift
//  ToDo
//
//  Created by Igor Dev on 6/18/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

struct User: Codable {
    var email: String?
    var uid: String

    enum CodingKeys: String, CodingKey {
        case email
        case uid
    }

    mutating func setEmail(_ email: String) {
        self.email = email
    }
}
