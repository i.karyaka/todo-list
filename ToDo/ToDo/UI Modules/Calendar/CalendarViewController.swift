//
//  CalendarViewController.swift
//  ToDo
//
//  Created by Igor Dev on 6/19/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

final class CalendarViewController: ViewController<CalendarRouter, CalendarViewModel> {

    override func localize() {
        super.localize()
        title = "Calendar"
    }
}
