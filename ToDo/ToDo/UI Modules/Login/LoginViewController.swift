//
//  LoginViewController.swift
//  ToDo
//
//  Created by Igor Dev on 6/17/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import RxSwift
import RxCocoa
import RxKeyboard

class LoginViewController: ViewController<LoginRouter, LoginViewModel> {

    var emailField  = RoundedTextField(style: .bordered)
    var logInButton = RoundedButton()
    var signUpButton = RoundedButton()

    override func setupConstraints() {
        super.setupConstraints()

        view.addSubview(emailField)
        emailField.snp
            .makeConstraints { (make) in
                make.center.equalToSuperview()
                make.left.equalToSuperview().inset(16)
                make.right.equalToSuperview().inset(16)
                make.height.equalTo(42)
        }

        view.addSubview(logInButton)
        logInButton.snp
            .makeConstraints { (make) in
                make.top.equalTo(emailField.snp.bottom).offset(16)
                make.centerX.equalTo(emailField)
                make.height.equalTo(emailField)
                make.width.equalTo(80)
        }

        view.addSubview(signUpButton)
        signUpButton.snp
            .makeConstraints { (make) in
                make.bottom.equalTo(view.snp.bottomMargin).offset(16)
                make.centerX.equalTo(logInButton)
                make.height.equalTo(logInButton)
                make.width.equalTo(100)
        }
    }

    override func setupView() {
        super.setupView()
        view.backgroundColor = .black

        emailField.textContentType = .emailAddress
        emailField.borderWidth = 1
        emailField.borderColor = .darkGray
        emailField.textColor = .lightText
        emailField.placeholderColor = .lightGray
        emailField.textAlignment = .center

        logInButton.titleLabel?.font = .helveticaFontOf(size: 16)
        logInButton.setTitleColor(.lightText, for: .normal)

        signUpButton.titleLabel?.font = .helveticaFontOf(size: 16)
    }

    override func setupRx() {
        super.setupRx()

        viewModel.email
            .bind(to: emailField.rx.text)
            .disposed(by: disposeBag)

        emailField.rx
            .text.orEmpty
            .bind(to: viewModel.email)
            .disposed(by: disposeBag)

        logInButton.rx.action = viewModel.logInAction

        RxKeyboard.instance
            .visibleHeight
            .drive(onNext: { [weak self] height in
                self?.emailField.snp.updateConstraints({ make in
                    make.centerY.equalToSuperview().offset(-height / 2)
                })
            })
            .disposed(by: disposeBag)

        listenKeyboardUpdates()
    }

    override func localize() {
        super.localize()
        navigationItem.title = R.string.localizable.logInScreenTitle()
        emailField.placeholder = R.string.localizable.logInTextFieldPlaceholder()
        logInButton.setTitle(R.string.localizable.logInButtonTitle(),
                             for: .normal)
    }
}
