//
//  LoginViewModel.swift
//  ToDo
//
//  Created by Igor Dev on 6/17/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import RxSwift
import RxCocoa
import Action

class LoginViewModel: ViewModel<LoginRouter> {

    var email: BehaviorRelay<String> = BehaviorRelay(value: "")

    override init() {
        super.init()
        do {
            let savedMail: String = try services.userDefaults.value(forKey: KeyStorage.savedUserEmailKey)
            email.accept(savedMail)
        } catch {
            guard let sError = error as? ServiceError else {
                print(error.localizedDescription)
                return
            }
            router.errorHandler.serviceErrors.on(.next(sError))
        }
    }

    lazy var logInAction = CocoaAction(enabledIf: router.navigationAvailable.asObservable()) { [weak self] in
        guard let self = self else { return .empty() }
        return self.services.auth.logIn(email: "mail")
            .map { [weak self] _ in
                guard let self = self else { return }
                self.router.route(to: .mainNavigation)
        }
    }
}
