//
//  LoginRouter.swift
//  ToDo
//
//  Created by Igor Dev on 6/17/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import UIKit

class LoginRouter: Router {

    enum Route {
        case mainNavigation
    }

    func route(to route: Route) {
        switch route {
        case .mainNavigation: routeMainNavigation()
        }
    }

    private func routeMainNavigation() {
        let taskList    = Screens.taskList()
        let calendar    = Screens.calendar()
        let createTask  = Screens.createTask()
        let userInfo    = Screens.userInfo()
        let settings    = Screens.settings()

        let tabBarViewModel = MainTabBarViewModel(viewControllers: [taskList, calendar, createTask, userInfo, settings])
        let tabBarViewController = MainTabBarViewController(viewModel: tabBarViewModel)

        let navigation = Screens.navigation(root: tabBarViewController)

        guard let delegate = UIApplication.shared.delegate,
            let window = delegate.window else { return }

        if let fromVC = window?.rootViewController {
            navigation.view.frame = fromVC.view.frame
            UIView.transition(
                from: fromVC.view,
                to: navigation.view,
                duration: 0.33,
                options: [.transitionCrossDissolve, .curveEaseOut],
                completion: { _ in
                    window?.rootViewController = navigation
            }
            )
        } else {
            window?.rootViewController = navigation
        }
    }
}
