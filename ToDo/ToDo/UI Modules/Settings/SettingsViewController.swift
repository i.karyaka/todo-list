//
//  SettingsViewController.swift
//  ToDo
//
//  Created by Igor Dev on 6/19/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

final class SettingsViewController: ViewController<SettingsRouter, SettingsViewModel> {

    override func localize() {
        super.localize()
        title = "Settings"
    }
}
