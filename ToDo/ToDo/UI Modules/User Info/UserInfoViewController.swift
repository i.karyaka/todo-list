//
//  UserInfoViewController.swift
//  ToDo
//
//  Created by Igor Dev on 6/19/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

final class UserInfoViewController: ViewController<UserInfoRouter, UserInfoViewModel> {

    override func localize() {
        super.localize()
        title = "User"
    }
}
