//
//  TaskListViewController.swift
//  ToDo
//
//  Created by Igor Dev on 6/18/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import RxSwift
import RxDataSources

final class TaskListViewController: ViewController<TaskListRouter, TaskListViewModel>, UIScrollViewDelegate {

    let tableView = UITableView()

    var dataSource: RxTableViewSectionedReloadDataSource<TaskListViewModel.Section>?

    override func setupConstraints() {
        super.setupConstraints()

        view.addSubview(tableView)
        tableView.snp
            .makeConstraints { make in
                make.edges.equalToSuperview()
        }
    }

    override func setupTable() {
        super.setupTable()
        tableView.tableFooterView = UIView()
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 220
        tableView.rowHeight = UITableView.automaticDimension

        disposeBag.insert(
            Observable.just([ UITableViewCell.self ])
                .bind(to: tableView.rx.cellTypes),
            viewModel.sections.bind(to: tableView.rx.items(dataSource: dataSource!)),
            tableView.rx.setDelegate(self)
        )
    }

    override func setupDataSource() {
        super.setupDataSource()

        dataSource = RxTableViewSectionedReloadDataSource<TaskListViewModel.Section> (
            configureCell: { [weak self] (_, tableView, indexPath, cellModel) -> UITableViewCell in
                guard let self = self else { return UITableViewCell() }
                return self.makeCell(cellModel, tableView: tableView, indexPath: indexPath)
        })

        disposeBag.insert(
            tableView.rx
                .modelSelected(TaskListViewModel.Cell.self)
                .bind(to: viewModel.openTask.inputs)
        )
    }

    private func makeCell(_ cellModel: TaskListViewModel.Cell,
                          tableView: UITableView,
                          indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: UITableViewCell.identifier,
                                                       for: indexPath) as? UITableViewCell
            else { return UITableViewCell() }

        switch cellModel.type {
        case .default:
            #warning("bind properties to cell")
//            guard let scenario = cellModel.item else { break }
        }

//        disposeBag.insert(
//            cell.layoutUpdated.subscribe(onNext: {
//                tableView.reloadData()
//            })
//        )

        return cell
    }

    override func localize() {
        super.localize()
        title = R.string.localizable.taskListTitle()
    }
}
