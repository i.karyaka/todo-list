//
//  TaskListRouter.swift
//  ToDo
//
//  Created by Igor Dev on 6/18/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

class TaskListRouter: Router {
    enum Route {
        case task(Task)
    }

    func route(to route: Route) {
        switch route {
        case .task(let task): routeTask(task)
        }
    }

    private func routeTask(_ task: Task) {
        let controller = Screens.taskDetail(task)
        viewController?.navigationController?.pushViewController(controller, animated: true)
    }
}
