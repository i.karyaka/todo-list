//
//  TaskListViewModel+Section.swift
//  ToDo
//
//  Created by Igor Dev on 6/19/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import RxDataSources

extension TaskListViewModel {

    enum CellType {
        case `default`
    }

    class Cell: Equatable, IdentifiableType {

        var identity: String {
            return item.title
        }

        var item: Task
        private (set) var type: CellType = .default

        init(item: Task) {
            self.item = item
        }

        static func == (lhs: TaskListViewModel.Cell, rhs: TaskListViewModel.Cell) -> Bool {
            return lhs.item.title == rhs.item.title
        }
    }

    struct Section: SectionModelType {

        var items: [Cell]

        init(items: [Cell]) {
            self.items = items
        }

        init(original: Section, items: [Cell]) {
            self = original
            self.items = items
        }
    }
}
