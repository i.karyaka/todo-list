//
//  TaskListViewModel.swift
//  ToDo
//
//  Created by Igor Dev on 6/18/19.
//  Copyright © 2019 sowonderfultodo. All rights reserved.
//

import RxCocoa
import Action

class TaskListViewModel: ViewModel<TaskListRouter> {

    var sections: BehaviorRelay<[Section]> = BehaviorRelay(value: [])

    override init() {

        super.init()

        #warning("fill sections here")
    }

    lazy var openTask = Action<Cell, Void> { [weak self] (cellModel) in
        guard let self = self else { return .empty() }
        self.router.route(to: .task(cellModel.item))
        return .empty()
    }
}
